const burgerButton = document.querySelector(".burger-button");
const headerNav = document.querySelector(".header-nav");

burgerButton.addEventListener("click", () => {
  burgerButton.classList.toggle("burger-button--active");
  headerNav.classList.toggle("header-nav--open");
});
